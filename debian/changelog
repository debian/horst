horst (5.1-3) unstable; urgency=medium

  [ Antoine Beaupré ]
  * acknowledge NMU
  * update watch file to latest uscan(1) examples

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)
  * Update standards version to 4.1.1, no changes needed.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Set debhelper-compat version in Build-Depends.
  * Bump debhelper from old 10 to 12.

 -- Antoine Beaupré <anarcat@debian.org>  Sat, 04 Feb 2023 14:21:20 -0500

horst (5.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch from Sven Joachim to fix FTBFS with new ncurses.
    (Closes: #995202)

 -- Adrian Bunk <bunk@debian.org>  Sun, 28 Nov 2021 20:51:58 +0200

horst (5.1-2) unstable; urgency=medium

  * Bug fix: "horst FTCBFS: upstream Makefile hard codes build
    architecture tools", thanks to Helmut Grohne (Closes: #920780).

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 01 Feb 2019 10:29:06 -0500

horst (5.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/rules: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

  [ Antoine Beaupré ]
  * new upstream release (Closes: #917333, #889472)
  * follow upstream tag naming changes in gbp.conf and watch file
  * disable sparse checks as they now fail everywhere (Closes: #911102)
  * tweak upstream Makefile to follow standards (reported upstream)
  * fix control file syntax error

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 09 Jan 2019 20:34:06 -0500

horst (5.0-2) unstable; urgency=medium

  * fix gbp branch naming scheme
  * disable checks as sparse fails on some architectures, filed as #873508

 -- Antoine Beaupré <anarcat@debian.org>  Mon, 28 Aug 2017 10:21:01 -0400

horst (5.0-1) unstable; urgency=medium

  * merge with upstream git so drop overlay mechanism
  * upstream homepage moved
  * add new upstream dependencies
  * refresh description from upstream README
  * follow anonscm redirect
  * follow upstream docs removal and rename
  * run upstream tests again as that multi-arch bug was fixed
  * sparse now in main, follow suit
  * bump dh compat to 10, no change
  * bump standards to 4.1.0, no change

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 23 Aug 2017 14:17:45 -0400

horst (4.2-1) unstable; urgency=medium

  * move to contrib because of non-free dependency (Closes: #763566)
  * new upstream release (Closes: #749664)
  * revert to older watchfile as (notified) upstream broke directory listing
  * update standards version, no change

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 09 Sep 2015 20:47:22 -0400

horst (4.0-1) unstable; urgency=low

  * new upstream release
  * use upstream manpage
  * add sparse build-depend
  * skip upstream sparse test because of #755979
  * upstream seems to have fixed arm64 issues, but i'll let the buildds
    confirm (Closes: #758624)

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 26 Aug 2014 15:06:42 -0700

horst (3.0-2) unstable; urgency=low

  * add missing copyright files, thanks Bruno Randolf for the huge help
  * simplify watch file to use HTTP listing
  * refer to the README file in manpage
  * update manpage from upstream, with small spelling fixes
  * remove our README.Debian file now that the manpage is complete

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 15 Aug 2012 20:08:58 -0400

horst (3.0-1) unstable; urgency=low

  * Initial release (Closes: #684440)

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 09 Aug 2012 22:27:34 -0400

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: horst
Source: http://br1.einfach.org/tech/horst/

Files: *
Copyright: 2005-2011 Bruno Randolf <br1@einfach.org>
License: GPL-2.0+

Files: average.*
Source: First committed to linux kernel
Copyright: 2010 Bruno Randolf
License: GPL-2.0

Files: batman_header.h
Source: batman batman.h
Copyright: 2006 B.A.T.M.A.N. contributors: Thomas Lopatic, Corinna
           'Elektra' Aichele, Axel Neumann, Marek Lindner
License: GPL-2.0

Files: capture-pcap.c
Copyright: 2005-2011 Bruno Randolf (br1@einfach.org)
           2007 Sven-Ola Tuecke
License: GPL-2.0

Files: ieee80211_util.*
Source: copied from linux wireless-2.6/net/mac80211/util.c
Copyright: 2002-2005, Instant802 Networks, Inc.
           2005-2006, Devicescape Software, Inc.
           2006-2007	Jiri Benc <jbenc@suse.cz>
           2007	Johannes Berg <johannes@sipsolutions.net>
License: GPL-2.0

Files: ccan/list/list.h
Copyright: 2005 Linus Torvalds
Source: Linux kernel
Comment: Not sure who is the copyright holder, maybe Linus Torvalds?
         See: http://lkml.indiana.edu/hypermail/linux/kernel/0508.3/1309.html
License: GPL-2.0

Files: listsort.c
Copyright: 2001 Simon Tatham
           2005-2011 Bruno Randolf (br1@einfach.org)
Comment: originally Expat, additions by Bruno Randolf are GPL-2.0
License: Expat and GPL-2.0

Files: olsr_header.h
Copyright: 2004, Andreas Tønnesen(andreto@olsr.org)
Source: copied from olsr olsr_protocol.h
License: BSD-3-clause

Files: prism_header.h
Copyright: 2005 John Bicket
Source: copied from madwifi net80211/ieee80211_monitor.h
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2.0
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL SIMON TATHAM BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2012 Antoine Beaupré <anarcat@debian.org>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

